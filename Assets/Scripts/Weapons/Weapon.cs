﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class Weapon : ShipSystem {

	//Regulate firing the guns
	private bool isFiring;
	private GameObject Projectile;
	public List<GameObject> Projectiles;
	private Rigidbody2D parentVelocity;
	private float nextFire;
	private int barrelAlt = 1;
	public Transform shotSpawn, shotSpawn2, shotSpawn3, shotSpawn4;
	public int numBarrels;
	Vector3 pos = new Vector3(0,0,0);
	Quaternion rot = new Quaternion(0,0,0,0);

	//magazine stuff
	private int shotsInMag;
	private float nextMagReload;

	//Weapon stats
	public float TurretRotationSpeed;
	public int MagazineSize;
	public float SecondsBetweenShots;
	public float SecondsToReload;
	public float EnergyConsumedPerShot;
	public int turretArc;
	public float Accuracy;

	//Projectile Stats
	//A value of 1.0 is default. 0.75 is 75% of normal, etc.
	public float PROJ_MaxRangeModifier; 
	public float PROJ_SpeedModifier;
	public float PROJ_EffectiveRangeModifier; 
	public float PROJ_DamageModifier;

	public int PROJ_Caliber; // Determines shell size and damage.


	public void StartFiring(){
		isFiring = true;
	}

	public void StopFiring(){
		isFiring = false;
	}

	void Update(){
		if (Projectiles.Count <= 0) {
			Debug.LogError ("A weapon is trying to shoot without any ammo types avaliable! Weapon = " + gameObject.name);
			return;
		}
		if (Projectile == null) {
			Projectile = Projectiles [0];
		}
		if (Time.time > nextMagReload && shotsInMag == 0) 
			shotsInMag = MagazineSize;

		if (isFiring && Time.time > nextFire && shotsInMag > 0) {
			
			parentVelocity = transform.parent.GetComponent<Rigidbody2D> ();
			nextFire = Time.time + SecondsBetweenShots;
			GameObject g;

			//Alternate fire between barrels, if there's more than one.
			if (barrelAlt == 1 && shotSpawn2 != null) {
				pos = shotSpawn.position;
				rot	= shotSpawn.rotation;
			}
			if (barrelAlt == 2 && shotSpawn2 != null) {
				pos = shotSpawn2.position;
				rot	= shotSpawn2.rotation;
			}
			if (barrelAlt == 3 && shotSpawn2 != null) {
				pos = shotSpawn3.position;
				rot	= shotSpawn3.rotation;
			}
			if (barrelAlt == 4 && shotSpawn2 != null) {
				pos = shotSpawn4.position;
				rot	= shotSpawn4.rotation;
			}
			//Spawn our projectile
			rot.z = rot.z + Random.Range(-Accuracy,Accuracy);
			g = Instantiate (Projectile,pos,rot) as GameObject;
			ProjectileBase gProjectile = g.GetComponent<ProjectileBase> ();
			gProjectile.wep = this;
			gProjectile.Fire();
			g.GetComponent<Rigidbody2D> ().velocity = (parentVelocity.velocity/2);


			shotsInMag--;
			if (shotsInMag == 0) {
				nextMagReload = Time.time + SecondsToReload;
			}
			//Alternating barrels stuff.
			if (barrelAlt < numBarrels) {
				barrelAlt++;
			} else {
				barrelAlt = 1;
			}
		}
	}
}
