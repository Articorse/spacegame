using UnityEngine;
using System.Collections;

public class ProjectileBase : MonoBehaviour {
	public Weapon wep;
	public Rigidbody2D rb;
	public float speed;
	public GameObject Explosion;
	protected BoxCollider2D damage_hitbox;
	protected float MaxRange, EffectiveRange;
	protected int BaseDamage, FinalDamage;
	protected bool isAlive = false;
	private bool ReducedEffective = false;
	private float fireTime;

	// Use this for initialization
	public virtual void Fire () {
		rb = GetComponent<Rigidbody2D> ();
		rb.AddForce (transform.up * (speed * wep.PROJ_SpeedModifier));
		fireTime = Time.time;
		Resize();
		damage_hitbox = gameObject.GetComponent<BoxCollider2D>();
		isAlive = true;
	}

	protected void Resize(){
		GetComponent<Transform> ().localScale = new Vector3((wep.PROJ_Caliber / 10)+1, (wep.PROJ_Caliber / 10)+1,(wep.PROJ_Caliber / 10)+1);
		EffectiveRange = wep.PROJ_EffectiveRangeModifier * wep.PROJ_Caliber * 5;
		MaxRange = wep.PROJ_MaxRangeModifier * wep.PROJ_Caliber * 7;
		BaseDamage = wep.PROJ_Caliber * 3 / 2;
		FinalDamage = BaseDamage;
		FinalDamage = FinalDamage + (int)(FinalDamage * Random.Range (-0.25f, 0.25f)); // Damage randomization for each shell. 25% higher or lower than base damage
		speed = wep.PROJ_SpeedModifier * speed;
	}

	public virtual void Explode(){
		if (!isAlive)
			return;
		GameObject expl = (GameObject)Instantiate (Explosion);
		expl.transform.position = transform.position;
		expl.transform.rotation = Quaternion.Euler(0.0f,0.0f,Random.Range(0.0f, 360.0f));
		expl.GetComponent<Explosion> ().Detonate (wep.PROJ_Caliber,FinalDamage);
		Destroy (gameObject);
	}
		
	protected virtual void FixedUpdate() {
		if (!isAlive)
			return;
		if (fireTime + EffectiveRange < Time.time && !ReducedEffective) {
			ReducedEffective = true; 
			ReducedEffectiveness();
		}

		if (fireTime + MaxRange < Time.time) {
			Explode();
		}
	}

	protected virtual void ReducedEffectiveness(){
		rb.AddForce (transform.up * -speed);
	}

	protected virtual int getDamage(){
		return BaseDamage;
	}
		
	protected virtual void OnTriggerEnter2D(Collider2D col){
		if (!isAlive)
			return;
		if (col.gameObject.transform.parent.tag == "Player")
			return;

			Explode ();
	}
}
