﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {
	public GameObject debugText;

	Transform orientation;
	Weapon baseWep;
	void Awake(){
		orientation = transform.parent;
		baseWep = GetComponentInParent<Weapon> ();

	}

	void FixedUpdate () {
		Vector3 lookDirection = orientation.InverseTransformPoint (Camera.main.ScreenToWorldPoint ((Vector3)Input.mousePosition)) - transform.localPosition;

		float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
		if (baseWep.turretArc != -1) angle = ClampAngle (angle, -baseWep.turretArc, baseWep.turretArc);

		Quaternion LocalRotation = Quaternion.AngleAxis(angle, transform.forward);

		transform.localRotation = Quaternion.RotateTowards(transform.localRotation, LocalRotation, baseWep.TurretRotationSpeed);

	}

	private float ClampAngle(float angle, float min, float max){
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;

		return Mathf.Clamp (angle, min, max);
	}
}
