﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	protected CircleCollider2D Radius;

	protected int Damage;
	protected bool cutoff = false; // explosion shouldn't drag out over more than an instant.
	// Use this for initialization
	public void Detonate(int caliber, int dmg) {
		transform.localScale = new Vector3(caliber*1.5f,caliber*1.5f,caliber*1.5f);
		Radius = GetComponent<CircleCollider2D> ();
		Damage = dmg;
	}

	protected virtual void OnTriggerEnter2D(Collider2D col){
		if (!cutoff && col.transform.GetComponent<ShipSegment>() != null) col.transform.GetComponent<ShipSegment> ().TakeDamage (Damage);
	}

	public void DoDamage(){
		cutoff = true;
	}
				

	public void EndAnimation(){
		Destroy (gameObject);
	}
}
