﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ShipSelection : MonoBehaviour {

	public void Play () {
		GameObject gameCore = GameObject.FindGameObjectWithTag ("GameCore");
		var globalCore = gameCore.GetComponent<GlobalCore> ();
		SceneManager.LoadScene ("Main");
	}
}