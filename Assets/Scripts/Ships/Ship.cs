﻿// I was thinking of adding something witty somewhere for Bunny to find, but it's 4:06 AM and my brain is mush.
// So I'll just say I <3 Bunny very much and hope he has an easy time understanding my explanations~


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Ship : MonoBehaviour {

	public string shipName;

	private float _X = 0; // The ship's current location on the X axis.
	private float _Y = 0; // The ship's current location on the Y axis.
	private Rigidbody2D rBody;
	private PolygonCollider2D shipCollider;
	private List<Vector2> colCenters = new List<Vector2> (); // A list of the center points of all the segments' colliders. Used to calculate the center of mass for the ship.
	public ShipSegment core; // The Core of the ship. Where the pilot/AI is. Without this, the ship is considered eliminated.
	private Grid grid;

	// Generators and Energy
	private List<Generator> generators = new List<Generator> (); // A list of all the ship's generators.
	private float energyGeneration = 0;
	public float energyCapacity = 0;
	public float curEnergy = 0;
	private float totalRotEnergy = 0; // How much energy is expended by the reaction wheels.

	// Thrusters
	private List<Thruster> thrusters = new List<Thruster> (); // A list of all the ship's thrusters.
	private Dictionary<string, float> topSpeed = new Dictionary<string, float> { // The total thrust that all thrusters provide in each direction.
		{StaticGameCore.top, 0},
		{StaticGameCore.bottom, 0},
		{StaticGameCore.left, 0},
		{StaticGameCore.right, 0}
	};

	//Fire control
	private List<Weapon> Weapons = new List<Weapon>();
	public bool Fire1Held = false;

	// Reaction Wheels
	private float totalTorque = 0; // The total rotational thrust the ship has.
	private List<ReactionWheel> rWheels = new List<ReactionWheel> ();

	// Vector Stabilizers
	private List<VectorStabilizer> vectStabilizers = new List<VectorStabilizer> (); // A list of all the vector stabilizers the ship has.
	private float totalVectStabilization = 0; // The total vector stabilization the ship has.
	private bool vectStabilizationOn = true; // Shows if the vector stabilization system is turned on.
	private float predictiveVectStab = 0; // Used to make sure the vector stabilization doesn't overshoot and make the ship move in the opposite direction.

	// Rotational Stabilizers
	private List<RotStabilizer> rotStabilizers = new List<RotStabilizer> (); // A list of all the rotational stabilizers the ship has.
	private float totalRotStablilization = 0; // The total amount of rotational stabilization the ship has.
	private float topRotSpeed = StaticGameCore.globalRotTopSpeed; // The top speed at which the ship can rotate.
	private bool rotStabilizationOn = true; // Shows if the rotational stabilization system is turned on.
	private float predictiveRotStab = 0; // Used to make sure the rotational stabilization doesn't overshoot and make the ship rotate in the opposite direction.

	// Makes sure the ship's slots, connections, facing, colliders and mass are properly initialized.
	void Awake () {
		grid = transform.GetComponent<Grid> ();
		rBody = transform.GetComponent<Rigidbody2D> ();
		shipCollider = transform.GetComponent<PolygonCollider2D> ();
		Vector3 curPos = gameObject.transform.position;
		_X = curPos.x;
		_Y = curPos.y;
		TotalRefresh ();
		curEnergy = energyCapacity;
	}
	
	// Maintains the ship's current coordinates in the _X and _Y variables.
	void FixedUpdate () {
		_X = transform.position.x;
		_Y = transform.position.y;
		curEnergy += energyGeneration;
		curEnergy = Mathf.Clamp (curEnergy, 0f, energyCapacity);

		if (Fire1Held) {
			foreach (Weapon w in Weapons) { // Sums up all the torque that the ship's reaction wheels generate.
				w.StartFiring();
			}
		} else {
			foreach (Weapon w in Weapons) { // Sums up all the torque that the ship's reaction wheels generate.
				w.StopFiring();
			}
		}
	}

	// Get method for the x coordinate.
	public float x {
		get { return _X; }
	}

	// Get method for the y coordinate.
	public float y {
		get { return _Y; }
	}

	// Get/set method for the rotational stabilization system.
	public bool rotStab {
		get { return rotStabilizationOn; }
		set { rotStabilizationOn = value; }
	}

	// Get/set method for the vector stabilization system.
	public bool vectStab {
		get { return vectStabilizationOn; }
		set { vectStabilizationOn = value; }
	}

	// Toggles the rotational stabilization on and off. Useful for keybinding.
	public void ToggleRotStab () {
		rotStabilizationOn = !rotStabilizationOn;
	}

	// Toggles the vector stabilization on and off. Useful for keybinding.
	public void ToggleVectStab () {
		vectStabilizationOn = !vectStabilizationOn;
	}

	// Refreshes the ship's total thrust, rotational thurst, vector and rotational stabilization, top speed and top rotational speed.
	public void RefreshSystems () {
		
		// Thrusters
		thrusters = new List<Thruster> ();
		topSpeed [StaticGameCore.left] = StaticGameCore.globalTopSpeed;
		topSpeed [StaticGameCore.top] = StaticGameCore.globalTopSpeed;
		topSpeed [StaticGameCore.right] = StaticGameCore.globalTopSpeed;
		topSpeed [StaticGameCore.bottom] = StaticGameCore.globalTopSpeed;

		// Reaction Wheels
		rWheels = new List<ReactionWheel> ();
		totalTorque = 0;
		topRotSpeed = StaticGameCore.globalRotTopSpeed;
		totalRotEnergy = 0;


		// Vector Stabilization
		vectStabilizers = new List<VectorStabilizer> ();
		totalVectStabilization = 0;

		// Rotational Stabilization
		rotStabilizers = new List<RotStabilizer> ();
		totalRotStablilization = 0;

		// Energy generation and capacity
		generators = new List<Generator> ();
		energyGeneration = 0;
		energyCapacity = 0;

		// Weapons
		Weapons = new List<Weapon>();

		for (int i = 0; i < grid.segments.Count; i++) { // Goes through all the segments and collects all of their systems into the appropriate lists.
			Thruster thruster = grid.segments [i].GetComponent<Thruster> ();
			ReactionWheel rWheel = grid.segments [i].GetComponent<ReactionWheel> ();
			VectorStabilizer vStab = grid.segments [i].GetComponent<VectorStabilizer> ();
			RotStabilizer rStab = grid.segments [i].GetComponent<RotStabilizer> ();
			Generator gen = grid.segments [i].GetComponent<Generator> ();
			Weapon wep = grid.segments [i].GetComponent<Weapon> ();
			if (thruster != null) {
				thrusters.Add (thruster);
			}
			if (rWheel != null) {
				rWheels.Add (rWheel);
			}
			if (vStab != null) {
				vectStabilizers.Add (vStab);
			}
			if (rStab != null) {
				rotStabilizers.Add (rStab);
			}
			if (gen != null) {
				generators.Add (gen);
			}
			if (wep != null) {
				Weapons.Add (wep);
			}
		}
		foreach (Generator g in generators) {
			energyGeneration += g.energyGeneration;
			energyCapacity += g.energyCapacity;
		}
		foreach (ReactionWheel rw in rWheels) { // Sums up all the torque that the ship's reaction wheels generate.
			totalTorque += rw.torque;
			totalRotEnergy += rw.energyGeneration;
		}
		foreach (Thruster t in thrusters) { // Sets the top speeds for each direction by taking the lowest top speed out of all the thrusters pointed in the given direction.
			if (t.actDirection == Vector2.left) {
				if (t.topSpeed < topSpeed [StaticGameCore.left]) {
					topSpeed [StaticGameCore.left] = t.topSpeed;
				}
			} else if (t.actDirection == Vector2.up) {
				if (t.topSpeed < topSpeed [StaticGameCore.top]) {
					topSpeed [StaticGameCore.top] = t.topSpeed;
				}
			} else if (t.actDirection == Vector2.right) {
				if (t.topSpeed < topSpeed [StaticGameCore.right]) {
					topSpeed [StaticGameCore.right] = t.topSpeed;
				}
			} else if (t.actDirection == Vector2.down) {
				if (t.topSpeed < topSpeed [StaticGameCore.bottom]) {
					topSpeed [StaticGameCore.bottom] = t.topSpeed;
				}
			}
		}
		foreach (VectorStabilizer vs in vectStabilizers) {
			totalVectStabilization += vs.stabilization;
			energyGeneration += vs.energyGeneration;
		}
		foreach (RotStabilizer rs in rotStabilizers) {
			totalRotStablilization += rs.stabilization;
			energyGeneration += rs.energyGeneration;
			if (rs.topSpeed < topRotSpeed) {
				topRotSpeed = rs.topSpeed;
			}
		}
	}

	// Finds the mathematical center of a polygon. Used to find the center of colliders for the purposes of calculating the ship's center of mass.
	// * Should maybe be placed in the ShipSegment class?
	private Vector2 FindCenter(Vector2[] polygon) {
		Vector2 pos = Vector2.zero;
		foreach (Vector2 point in polygon) {
			pos += point;
		}
		Vector2 center = pos / polygon.Length;
		return center;
	}

	// Determines the ship's center of mass, as well as the mass itself.
	///////////////////////////////////////////////////
	// MUST BE CALLED ONLY AFTER REFRESHSYSTEMS (). ///
	///////////////////////////////////////////////////
	public void RefreshMass () {
		Vector2 center = FindCenter (colCenters.ToArray ());
		Vector2 CoM = center;
		float totalMass = 0f;
		foreach (ShipSegment seg in grid.segments) {
			totalMass += seg.GetComponent<Rigidbody2D> ().mass;
		}
		foreach (ShipSegment seg in grid.segments) {
			Vector2 relPos = center - seg.center;
			CoM -= relPos * seg.GetComponent<Rigidbody2D> ().mass / totalMass;

		}
		rBody.centerOfMass = CoM;
		rBody.mass = totalMass;
		predictiveRotStab = Mathf.Sqrt ((totalRotStablilization / rBody.mass) * Time.fixedDeltaTime);
		predictiveVectStab = (totalVectStabilization / rBody.mass) * Time.fixedDeltaTime;
	}

	// Creates the ship's total collider from the colliders of its segments.
	// * Needs to be reworked to create one solid collider polygon, instead of using the individual polygons of the segment colliders.
	// Unfortunately, I'm not sure it's possible.
	public void RefreshCollider ()
	{
		colCenters = new List<Vector2> ();
		shipCollider.pathCount = 0;

		// Gets the colliders from each segment, then creates a new collider path for the ship.
		foreach (ShipSegment seg in grid.segments) {
			var tCol = seg.GetComponent<PolygonCollider2D> ();
			for (int i = 0; i < tCol.pathCount; i++) {
				Vector2[] p = tCol.GetPath (i);
				shipCollider.pathCount += 1;

				// Finds the segments location relative to the ship to be used in placing the ship's new collider path where the segment is.
				Vector2 relPos = seg.transform.localPosition;

				// Finds the furthest the segments go from the core on the X and Y axis individually.
				float maxX = 0;
				float maxY = 0;
				for (int j = 0; j < p.Length; j++) {
					if (p [j].x > maxX) {
						maxX = p [j].x;
					}
					if (p [j].y > maxY) {
						maxY = p [j].y;
					}
				}

				// Using the above two, calculates where each point on each new collider path should be located when taking into account segment mirroring and rotation.
				for (int j = 0; j < p.Length; j++) {
					if (seg._MirrorX) {
						p [j] = new Vector2 (-p [j].x, p [j].y);
					}
					if (seg._MirrorY) {
						p [j] = new Vector2 (p [j].x, -p [j].y);
					}
					if (seg.rotation == StaticGameCore.left) {
						p [j] = new Vector2 (-p [j].y, p [j].x);
					} else if (seg.rotation == StaticGameCore.bottom) {
						p [j] = new Vector2 (maxX - p [j].x, maxY - p [j].y);
					} else if (seg.rotation == StaticGameCore.right) {
						p [j] = new Vector2 (p [j].y, -p [j].x);
					}
					p [j] += relPos;
				}

				// Sets the current path as a new path for the ship's collider.
				int curPath = shipCollider.pathCount - 1;
				shipCollider.SetPath (curPath, p);

				// Finds the center of the segment and loads it into the colCenters list.
				seg.center = FindCenter (p);
				colCenters.Add (seg.center);
			}
		}
	}

	// Applies thrust to the ship in the given direction.
	public void Accelerate (string direction) {

		// If vector stabilization is off, or it's on and the ship is being accelerated forwards or backswards,
		// turns the local directional vector global, then applies force in the direction equal to the total thrust
		// the ship has in the given direction.
		if (!vectStabilizationOn || (vectStabilizationOn && direction == StaticGameCore.top) || (vectStabilizationOn && direction == StaticGameCore.bottom)) {
			Vector2 dir = rBody.GetVector (StaticGameCore.vectDict [direction]);
			dir.x = dir.x * -1;


			foreach (Thruster thruster in thrusters) {
				if (thruster.actDirection == StaticGameCore.vectDict [direction]) {
					Vector2 thrustSource = (Vector2)thruster.transform.position + (Vector2)(transform.rotation * thruster.relativeThrustSource);
					if (curEnergy >= Mathf.Abs(thruster.energyGeneration)) {
						curEnergy += thruster.energyGeneration;
						rBody.AddForceAtPosition (dir * thruster.thrust, thrustSource);
					}
				}
			}

			// If the ship's speed in the given direction is as much or higher than its top speed in that direction,
			// sets the ship's speed to be equal to the top speed.
			if (rBody.velocity.magnitude >= topSpeed [direction]) {
				rBody.velocity = rBody.velocity.normalized * topSpeed [direction];
			}
		}
	}

	// Applies torque to the ship in the given direction.
	public void Rotate (float input) {
		float curRot = rBody.angularVelocity;

		// If the current rotational velocity (taken as an absolute value so rotation in the - direction also counts)
		// is less than or equal to the game's global top rotational speed,
		// continues as normal. If it's higher, it applies thrust in the opposite
		// direction of rotation.
		if (Mathf.Abs (curRot) <= StaticGameCore.globalRotTopSpeed) {

			// If rotational stabilization is on, proceeds as normal. If it's off, ignores the ship's
			// top rotational speed and applies torque until the global top rotational speed is reached.
			if (rotStabilizationOn) {

				// If the current rotational velocity (absolute value) is less than the top rotational
				// speed of the ship, adds torque. If it's higher, it applies thrust in the opposite direction.
				if (Mathf.Abs (curRot) < topRotSpeed) {
					if (curEnergy >= Mathf.Abs(totalRotEnergy)) {
						curEnergy += totalRotEnergy;
						rBody.AddTorque (totalTorque * input);
					}
				} else {
					float curRotNorm = curRot / Mathf.Abs (curRot);
					rBody.AddTorque (-totalTorque * curRotNorm);
				}
			} else {
				rBody.AddTorque (totalTorque * input);
			}
		} else {
			float curRotNorm = curRot / Mathf.Abs (curRot);
			rBody.AddTorque (-totalTorque * curRotNorm);
		}
	}

	// Limits the rotational speed of the ship. If rotational stabilization is on, makes sure it's
	// below the ship's top rotational speed, and if it's not, makes sure it's below the global
	// top rotational speed.
	public void LimitRotation () {
		float curRot = rBody.angularVelocity;
		if (rotStabilizationOn) {
			if (curRot > topRotSpeed) {
				rBody.angularVelocity = topRotSpeed;
			} else if (curRot < -topRotSpeed) {
				rBody.angularVelocity = -topRotSpeed;
			}
		} else {
			if (curRot > StaticGameCore.globalRotTopSpeed) {
				rBody.angularVelocity = StaticGameCore.globalRotTopSpeed;
			} else if (curRot < -StaticGameCore.globalRotTopSpeed) {
				rBody.angularVelocity = -StaticGameCore.globalRotTopSpeed;
			}
		}
	}

	// Applies a brake to the current rotation when necessary.
	public void StabilizeRotation ()
	{
		if (rotStabilizationOn) {
			float input = Input.GetAxis ("Horizontal");
			float curRot = rBody.angularVelocity;

			// If the ship is currently not trying to rotate, or is trying to rotate in the direction
			// opposite of it's current rotation, the rotational stabilization kicks in and applies
			// a force in the opposite direction of the current rotation.
			if (input == 0 || (input > 0 && curRot <= 0) || (input < 0 && curRot >= 0)) {

				// If rotational stabilization is on, checks if the current rotation is over a global
				// threshold. If it is, it gradually reduces it. If it's under it, it immediately
				// sets it to 0. This is so the ship won't continue to rotate very slowly for a long time
				// after you let go of the button. This is so the ship won't continue to slowly rotate
				// for a long time after you want it to stop.
				if (predictiveRotStab < Mathf.Abs (curRot) && Mathf.Abs (curRot) > StaticGameCore.rotStabilizationTreshold) {
					rBody.AddTorque (-(Mathf.Abs (curRot) / curRot) * totalRotStablilization);
				} else {
					rBody.angularVelocity = 0;
				}
			}
		}
	}

	// Stabilizes the vector of movement for the ship.
	public void StabilizeVector () {
		if (vectStabilizationOn) {
			Vector2 curHeading = rBody.velocity;

			// If the current speed is over a certain threshold, proceeds as normal.
			// If it's under it, sets the velocity to zero.
			// I use sqrMagnitude instead of magnitude, because magnitude is
			// simply a square root of sqrMagnitude, thus decreasing the work of the CPU.
			if (Mathf.Abs (curHeading.sqrMagnitude) > StaticGameCore.vectStabilizationTresholdSqr) {
				Vector2 curFwd = rBody.GetVector (Vector2.up);
				curFwd.x = -curFwd.x; // For some reason, Unity flips the X axis and I have to correct for it here.
				Vector2 localHeading = transform.InverseTransformDirection (curHeading);

				// Applies force equal to the total vector stabilization against the ship's current vector of movement.
				rBody.AddForce (-curHeading.normalized * totalVectStabilization);

				// Applies the force taken from the above line to the ship's current direction of movement.
				// Doesn't work for strafing.
				if (localHeading.y >= 0) {
					rBody.AddForce (curFwd * totalVectStabilization);
				} else {
					rBody.AddForce (-curFwd * totalVectStabilization);
				}

			} else {
				rBody.velocity = Vector2.zero;
			}
		}
	}

	// Vector stabilization for when the ship isn't currently accelerating, or is accelerating in the opposite direction
	// that the ship is currently moving in.
	public void PassiveVectorStabilization () {
		if (vectStabilizationOn) {
			Vector2 curHeading = rBody.velocity;
			float input = Input.GetAxis ("Vertical");
			Vector2 localHeading = transform.InverseTransformDirection (curHeading);

			// Applies a force opposite to the current direction of movement if the ship
			// is currently not accelerating, or is accelerating in the opposite direction
			// of which it is moving.
			if (input == 0 || (input > 0 && localHeading.y <= 0) || (input < 0 && localHeading.y >= 0)) {

				// This part's pretty fun. It calculates whether the braking force that the vector stabilization
				// provides will overshoot and make the ship start moving in the opposite direction.
				// If it calculates that it will, it simply sets the ship's velocity to 0.
				// Otherwise it continues applying the breaking force.
				if (predictiveVectStab * predictiveVectStab < curHeading.sqrMagnitude) {
					rBody.AddForce (-curHeading.normalized * totalVectStabilization);
				} else {
					rBody.velocity = Vector2.zero;
				}
			}
		}
	}

	// Refreshes the entire ship.
	public void TotalRefresh () {
		if (grid.segments.Count > 0) {
			foreach (ShipSegment seg in grid.segments) {
				seg.RefreshFacing ();
			}
			grid.PopulateSlots ();
			grid.RefreshConnections ();
			RefreshSystems ();
			RefreshCollider ();
			RefreshMass ();
		} else {
			foreach (Vector2 slotLoc in grid.GetOccupiedSlots) {
				grid.DestroySlot (slotLoc);
			}
		}
	}

	// Tries to add a segment to the ship. Returns the resulting segment if successful, returns null if the space is already occupied.
	// ALWAYS REFRESH THE SHIP AFTER USING THIS FUNCTION TO ADD ALL THE SEGMENTS YOU WANT.
	public ShipSegment AddSegment (StoredSegment stSeg) {
		List<Vector2> slotLocs = new List<Vector2> ();
		for (int i = 0; i < stSeg.seg._RelativeGridSlotLocations.Count; i++) {
			slotLocs.Add (stSeg.seg.location + stSeg.seg._RelativeGridSlotLocations [i]);
		}
		for (int i = 0; i < slotLocs.Count; i++) {
			if (grid.GetOccupiedSlots.Contains (slotLocs [i])) {
				return null;
			}
		}
		GameObject segObj = (GameObject)Instantiate (stSeg.prefab, stSeg.loc, Quaternion.Euler (0, 0, stSeg.rot), transform);
		ShipSegment newSeg = segObj.GetComponent<ShipSegment> ();
		newSeg.location = stSeg.loc;
		newSeg._Rotation = stSeg.rot;
		newSeg._MirrorX = stSeg.mirrorX;
		newSeg._MirrorY = stSeg.mirrorY;
		ShipSystem[] systems = newSeg.GetComponents<ShipSystem> ();
		foreach (ShipSystem sys in systems) {
			sys.InitCall ();
		}
		grid.segments.Add (newSeg);
		return grid.segments[grid.segments.Count-1];
	}

	public StoredShip StoreShip () {
		int stCore = 0;
		List<StoredSegment> stSegList = new List<StoredSegment> ();
		for (int i = 0; i < grid.segments.Count; i++) {
			StoredSegment stSeg = grid.segments[i].StoreSegment ();
			stSegList.Add (stSeg);
			if (core == grid.segments[i]) {
				stCore = i;
			}
		}
		StoredShip outputShip = new StoredShip ();
		outputShip.segments = stSegList;
		outputShip.core = stCore;
		return outputShip;
	}

	public void PopulateShip (StoredShip stShip) {
		grid.segments.Clear ();
		Transform[] children = GetComponentsInChildren<Transform> ();
		foreach (Transform c in children) {
			//
			//Destroy (c.gameObject); // FIX LATER
			//
		}
		foreach (StoredSegment stSeg in stShip.segments) {
			if (AddSegment (stSeg)) {
			} else {
				Debug.Log ("Couldn't add " + stSeg.seg);
			}
		}
		core = grid.segments[stShip.core];
		TotalRefresh ();
		curEnergy = energyCapacity;
	}

	public bool CheckIfShipIsValid () {
		if (core != null) {
			foreach (ShipSegment seg in grid.segments) {
				if (!grid.CheckIfConnectedToCore (seg)) {
					Debug.Log ("Not all segments are connected to the body of the ship.");
					return false;
				}
			}
		} else {
			Debug.Log ("Ship has no core.");
			return false;
		}
		if (energyGeneration <= 0) {
			Debug.Log ("Energy Generation is less than or equal to zero.");
			return false;
		} else {
			if (thrusters.Count <= 0) {
				Debug.Log ("WARNING: SHIP HAS NO THRUSTERS.");
			}
			if (totalTorque <= 0) {
				Debug.Log ("WARNING: SHIP HAS NO REACTION WHEELS.");
			}
			if (totalVectStabilization <= 0) {
				Debug.Log ("WARNING: SHIP HAS NO VECTOR STABILIZATION.");
			}
			if (totalRotStablilization <= 0) {
				Debug.Log ("WARNING: SHIP HAS NO ROTATIONAL STABILIZATION.");
			}
		}
		return true;
	}
}