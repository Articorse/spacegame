﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridSlot {

	private ShipSegment _Segment; // The GridSlot's parent segment.
	private Vector2 _Location; // The GridSlot's location.
	private List<bool> _SidesConnectable = new List<bool> (); // The list of sides that can be connected to other GridSlots, in the usual order of left, top, right, bottom.
	private List<GridSlot> _SidesConnectedTo = new List<GridSlot> (); // The list of GridSlots this GridSlot is currently connected to, in the usual order of left, top, right, bottom.

	public GridSlot (ShipSegment segm, Vector2 loc) {
		_Segment = segm;
		_Location = loc;
		_SidesConnectable.Add (false); // Left
		_SidesConnectable.Add (false); // Top
		_SidesConnectable.Add (false); // Right
		_SidesConnectable.Add (false); // Bottom
		_SidesConnectedTo.Add (null); // Left
		_SidesConnectedTo.Add (null); // Top
		_SidesConnectedTo.Add (null); // Right
		_SidesConnectedTo.Add (null); // Bottom
	}

	// Set method to make a certain side connectable or not.
	public void SetSideConnectable(string side, bool connectable) {
		if (side == StaticGameCore.left) {
			_SidesConnectable [0] = connectable;
		} else if (side == StaticGameCore.top) {
			_SidesConnectable [1] = connectable;
		} else if (side == StaticGameCore.right) {
			_SidesConnectable [2] = connectable;
		} else if (side == StaticGameCore.bottom) {
			_SidesConnectable [3] = connectable;
		} else {
			Debug.Log ("Invalid side selection.");
		}
	}

	// Set method to make a certain side connected to another GridSlot.
	public void SetSideConnected(string side, GridSlot connected) {
		if (side == StaticGameCore.left) {
			if (_SidesConnectable [0]) {
				_SidesConnectedTo [0] = connected;
			} else {
				_SidesConnectedTo [0] = null;
				Debug.Log ("Left Side not connectable.");
			}
		} else if (side == StaticGameCore.top) {
			if (_SidesConnectable [1]) {
				_SidesConnectedTo [1] = connected;
			} else {
				_SidesConnectedTo [1] = null;
				Debug.Log ("Top Side not connectable.");
			}
		} else if (side == StaticGameCore.right) {
			if (_SidesConnectable [2]) {
				_SidesConnectedTo [2] = connected;
			} else {
				_SidesConnectedTo [2] = null;
				Debug.Log ("Right Side not connectable.");
			}
		} else if (side == StaticGameCore.bottom) {
			if (_SidesConnectable [3]) {
				_SidesConnectedTo [3] = connected;
			} else {
				_SidesConnectedTo [3] = null;
				Debug.Log ("Bottom Side not connectable.");
			}
		} else {
			Debug.Log ("Invalid side selection.");
		}
	}

	public Vector2 location {
		get {
			return _Location;
		}
	}

	// Get method to see if a certain side is connectable.
	public bool IsSideConnectable (string side) {
		int s = 0;
		if (side == StaticGameCore.left) {
			s = 0;
		} else if (side == StaticGameCore.top) {
			s = 1;
		} else if (side == StaticGameCore.right) {
			s = 2;
		} else if (side == StaticGameCore.bottom) {
			s = 3;
		} else {
			Debug.Log ("Invalid side selection.");
			return false;
		}
		return _SidesConnectable [s];
	}

	// Get method to see what GridSlot a certain side is connected to.
	public GridSlot SideConnectedTo (string side) {
		int s = 0;
		if (side == StaticGameCore.left) {
			s = 0;
		} else if (side == StaticGameCore.top) {
			s = 1;
		} else if (side == StaticGameCore.right) {
			s = 2;
		} else if (side == StaticGameCore.bottom) {
			s = 3;
		} else {
			Debug.Log ("Invalid side selection.");
			return null;
		}
		return _SidesConnectedTo [s];
	}

	// Same as above, but takes an integer in the usual order of left, top, right, bottom;
	public GridSlot SideConnectedTo (int side) {
		if (side >= 0 && side <= 3) {
			return _SidesConnectedTo [side];
		} else {
			Debug.Log ("Invalid side selection.");
			return null;
		}
	}

	public ShipSegment GetSegment {
		get {
			return _Segment;
		}
	}
}