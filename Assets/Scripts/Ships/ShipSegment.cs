﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ShipSegment : MonoBehaviour
{
	public string segName;
	public string faction;
	public List<string> type;
	public int integrity; // The segment's hit points.
	private int currentIntegrity; // The segment's current hitpoints.
	public List<Vector2> _DefRelativeGridSlotLocations; // The default locations of all the GridSlots that make up this segment, relative to the location of the segment itself.
	public List<Vector2> _RelativeGridSlotLocations; // The final locations of all the GridSlots that make up this segment, relative to the location of the segment itself.
	public float _Rotation = 0; // The rotation of the segment. Only takes the values 0, 90, 180 and 270.
	public bool _MirrorX; // Whether or not the segment's X axis is flipped.
	public bool _MirrorY; // Whether or not the segment's Y axis is flipped.
	public Vector2 center = Vector2.zero; // The exact center of the segment. used to calculate the ship's center of mass.
	public List<int> _ConnectionPoints; // A complex variable that uses a single integer to tell the script which sides of each GridSlot are connectable and which aren't. Only used as an easy input in the editor.
	public List<List<bool>> conPoints; // The internal list of lists of booleans that show which sides of which GridSlot are connectable. Set from _ConnectionPoints in the RefreshFacing() function.
	public List<ShipSegment> neighbors = new List<ShipSegment> (); // A list of all the other segments this segment is connected to.
	public Vector2 location; // The location of the segment's main GridSlot.
	private Rigidbody2D rBody;
	private PolygonCollider2D col;
	private Grid grid;
	private DataBase db;

	public GameObject derelictObj;
	// The prefab gameobject that is instantiated when a derelict os split off.

	void Awake ()
	{
		_RelativeGridSlotLocations = new List<Vector2> ();
		for (int i = 0; i < _DefRelativeGridSlotLocations.Count; i++) {
			_RelativeGridSlotLocations.Add(new Vector2 (_DefRelativeGridSlotLocations[i].x, _DefRelativeGridSlotLocations[i].y));
		}
		rBody = GetComponent<Rigidbody2D> ();
		col = GetComponent<PolygonCollider2D> ();
		grid = GetComponentInParent<Grid> ();
		db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
		currentIntegrity = integrity;

		// This part makes sure the segment's rotation, scale and mirroring is set to 0/false so it doesn't influence the code that positions each segment.
		transform.eulerAngles = new Vector3 (0, 0, 0);
		transform.localScale = new Vector3 (0, 0, 0);
		SpriteRenderer sprite = GetComponent<SpriteRenderer> ();
		sprite.flipX = false;
		sprite.flipY = false;

		// This part finds the location corresponding to the segment in the Grid and applies it as an internal variable of the segment itself.
		if (grid != null) {
			for (int i = 0; i < grid.segments.Count; i++) {
				if (this == grid.segments [i]) {
					location = grid.locations [i];
				}
			}
		}
	}

	// This function makes sure the part is located where it's supposed to be,
	// is facing the way it should, makes it mirrored if necessary,
	// and sets which sides of which GridSlot are connectable,
	// based on _ConnectionPoints.
	public void RefreshFacing ()
	{
		conPoints = new List<List<bool>> ();
		for (int i = 0; i < _ConnectionPoints.Count; i++) {
			List<bool> curSlot = new List<bool> ();

			// Easy list of what value sets which sides as connectable:
			// 0 = NONE
			// 1 = LEFT
			// 2 = TOP
			// 3 = RIGHT
			// 4 = BOTTOM
			// 5 = LEFT AND TOP
			// 6 = LEFT AND RIGHT
			// 7 = LEFT AND BOTTOM
			// 8 = TOP AND RIGHT
			// 9 = TOP AND BOTTOM
			// 10 = RIGHT AND BOTTOM
			// 11 = LEFT, TOP AND RIGHT
			// 12 = LEFT, TOP AND BOTTOM
			// 13 = LEFT, RIGHT AND BOTTOM
			// 14 = TOP, RIGHT AND BOTTOM
			// 15 = ALL SIDES
			
			if (_ConnectionPoints [i] == 0) {
				curSlot.Add (false); // Left
				curSlot.Add (false); // Top
				curSlot.Add (false); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 1) {
				curSlot.Add (true); // Left
				curSlot.Add (false); // Top
				curSlot.Add (false); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 2) {
				curSlot.Add (false); // Left
				curSlot.Add (true); // Top
				curSlot.Add (false); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 3) {
				curSlot.Add (false); // Left
				curSlot.Add (false); // Top
				curSlot.Add (true); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 4) {
				curSlot.Add (false); // Left
				curSlot.Add (false); // Top
				curSlot.Add (false); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 5) {
				curSlot.Add (true); // Left
				curSlot.Add (true); // Top
				curSlot.Add (false); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 6) {
				curSlot.Add (true); // Left
				curSlot.Add (false); // Top
				curSlot.Add (true); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 7) {
				curSlot.Add (true); // Left
				curSlot.Add (false); // Top
				curSlot.Add (false); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 8) {
				curSlot.Add (false); // Left
				curSlot.Add (true); // Top
				curSlot.Add (true); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 9) {
				curSlot.Add (false); // Left
				curSlot.Add (true); // Top
				curSlot.Add (false); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 10) {
				curSlot.Add (false); // Left
				curSlot.Add (false); // Top
				curSlot.Add (true); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 11) {
				curSlot.Add (true); // Left
				curSlot.Add (true); // Top
				curSlot.Add (true); // Right
				curSlot.Add (false); // Bottom
			} else if (_ConnectionPoints [i] == 12) {
				curSlot.Add (true); // Left
				curSlot.Add (true); // Top
				curSlot.Add (false); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 13) {
				curSlot.Add (true); // Left
				curSlot.Add (false); // Top
				curSlot.Add (true); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 14) {
				curSlot.Add (false); // Left
				curSlot.Add (true); // Top
				curSlot.Add (true); // Right
				curSlot.Add (true); // Bottom
			} else if (_ConnectionPoints [i] == 15) {
				curSlot.Add (true); // Left
				curSlot.Add (true); // Top
				curSlot.Add (true); // Right
				curSlot.Add (true); // Bottom
			} else {
				curSlot.Add (false); // Left
				curSlot.Add (false); // Top
				curSlot.Add (false); // Right
				curSlot.Add (false); // Bottom
				Debug.Log ("Invalid Connection Point Value.");
			}
			conPoints.Add (curSlot);
		}
		for (int i = 0; i < conPoints.Count; i++) {
			List<bool> curSlot = conPoints [i];
			if (_MirrorX) { // If mirrored on the X axis, the slot's left and right connection points are flipped.
				bool temp = curSlot [0];
				curSlot [0] = curSlot [2];
				curSlot [2] = temp;
			}
			if (_MirrorY) { // If mirrored on the Y axis, the slot's top and bottom connection points are flipped.
				bool temp = curSlot [1];
				curSlot [1] = curSlot [3];
				curSlot [3] = temp;
			}
			if (rotation == StaticGameCore.left) { // if the rotation is left(90), top becomes left, right becomes top, bottom becomes right and left becomes bottom.
				bool temp = curSlot [0];
				curSlot [0] = curSlot [1];
				curSlot [1] = curSlot [2];
				curSlot [2] = curSlot [3];
				curSlot [3] = temp;
			} else if (rotation == StaticGameCore.bottom) { // if the rotation is bottom(180), top becomes bottom, bottom becomes top, left becomes right and left becomes right.
				bool temp = curSlot [0];
				bool temp2 = curSlot [1];
				curSlot [0] = curSlot [2];
				curSlot [2] = temp;
				curSlot [1] = curSlot [3];
				curSlot [3] = temp2;
			} else if (rotation == StaticGameCore.right) { // if the rotation is right(270), top becomes right, right becomes bottom, bottom becomes left and left becomes top.
				bool temp = curSlot [0];
				curSlot [0] = curSlot [3];
				curSlot [3] = curSlot [2];
				curSlot [2] = curSlot [1];
				curSlot [1] = temp;
			}
			conPoints [i] = curSlot;
		}

		_RelativeGridSlotLocations = new List<Vector2> ();
		for (int i = 0; i < _DefRelativeGridSlotLocations.Count; i++) {
			_RelativeGridSlotLocations.Add(new Vector2 (_DefRelativeGridSlotLocations[i].x, _DefRelativeGridSlotLocations[i].y));
		}
		for (int i = 0; i < _RelativeGridSlotLocations.Count; i++) {
			if (_Rotation == 90 || _Rotation == 270) { //This part fixes a strange issue I had with 90 and 270 degree rotations where the X and Y coordinates would get flipped.
				_RelativeGridSlotLocations [i] = -_RelativeGridSlotLocations [i];
			}
			if (_MirrorX) { // Flips the X axis for GridSlot locations if necessary.
				_RelativeGridSlotLocations [i] = new Vector2 (-_RelativeGridSlotLocations [i].x, _RelativeGridSlotLocations [i].y);
			}
			if (_MirrorY) { // Flips the Y axis for GridSlot locations if necessary.
				_RelativeGridSlotLocations [i] = new Vector2 (_RelativeGridSlotLocations [i].x, -_RelativeGridSlotLocations [i].y);
			}
		}

		transform.localEulerAngles = new Vector3 (0, 0, _Rotation); // Sets the rotation of the segment.

		// This part mirrors the segment if necessary.
		if (!_MirrorX && !_MirrorY) {
			transform.localScale = new Vector2 (1, 1);
		} else if (_MirrorX && !_MirrorY) {
			transform.localScale = new Vector2 (-1, 1);
		} else if (!_MirrorX && _MirrorY) {
			transform.localScale = new Vector2 (1, -1);
		} else {
			transform.localScale = new Vector2 (-1, -1);
		}


		float tempRot;
		StaticGameCore.rotDict.TryGetValue (StaticGameCore.left, out tempRot);
		if (_Rotation == tempRot) { // If the segment is facing left, reposition connection points and the collider.
			for (int i = 0; i < _RelativeGridSlotLocations.Count; i++) {
				float curX = _RelativeGridSlotLocations [i].x;
				float curY = _RelativeGridSlotLocations [i].y;
				_RelativeGridSlotLocations [i] = new Vector2 (curY, -curX);
				for (int j = 0; j < conPoints [i].Count; j++) {
					bool oldLeft = conPoints [i] [0];
					conPoints [i] [0] = conPoints [i] [1]; // New Left = Old Top
					conPoints [i] [1] = conPoints [i] [2]; // New Top = Old Right
					conPoints [i] [2] = conPoints [i] [3]; // New Right = Old Bottom
					conPoints [i] [3] = oldLeft; // New Bottom = Old Left
				}
				for (int j = 0; j < col.pathCount; j++) {
					for (int k = 0; k < col.GetPath (j).Length; k++) {
						float nodeX = col.GetPath (j) [k].x;
						float nodeY = col.GetPath (j) [k].y;
						col.GetPath (j) [k] = new Vector2 (nodeY, -nodeX);
					}
				}
			}
		} else {
			StaticGameCore.rotDict.TryGetValue (StaticGameCore.bottom, out tempRot);
			if (_Rotation == tempRot) { // If the segment is facing down, reposition connection points and the collider.
				for (int i = 0; i < _RelativeGridSlotLocations.Count; i++) {
					float curX = _RelativeGridSlotLocations [i].x;
					float curY = _RelativeGridSlotLocations [i].y;
					_RelativeGridSlotLocations [i] = new Vector2 (-curX, -curY);
					for (int j = 0; j < conPoints [i].Count; j++) {
						bool oldLeft = conPoints [i] [0];
						bool oldTop = conPoints [i] [1];
						conPoints [i] [0] = conPoints [i] [2]; // New Left = Old Right
						conPoints [i] [1] = conPoints [i] [3]; // New Top = Old Bottom
						conPoints [i] [2] = oldLeft; // New Right = Old Left
						conPoints [i] [3] = oldTop; // New Bottom = Old Top
					}
					for (int j = 0; j < col.pathCount; j++) {
						for (int k = 0; k < col.GetPath (j).Length; k++) {
							float nodeX = col.GetPath (j) [k].x;
							float nodeY = col.GetPath (j) [k].y;
							col.GetPath (j) [k] = new Vector2 (-nodeX, -nodeY);
						}
					}
				}
			} else {
				StaticGameCore.rotDict.TryGetValue (StaticGameCore.right, out tempRot);
				if (_Rotation == tempRot) { // If the segment is facing right, reposition connection points and the collider.
					for (int i = 0; i < _RelativeGridSlotLocations.Count; i++) {
						float curX = _RelativeGridSlotLocations [i].x;
						float curY = _RelativeGridSlotLocations [i].y;
						_RelativeGridSlotLocations [i] = new Vector2 (-curY, curX);
						for (int j = 0; j < conPoints [i].Count; j++) {
							bool oldLeft = conPoints [i] [0];
							conPoints [i] [0] = conPoints [i] [3]; // New Left = Old Bottom
							conPoints [i] [3] = conPoints [i] [2]; // New Bottom = Old Right
							conPoints [i] [2] = conPoints [i] [1]; // New Right = Old Top
							conPoints [i] [1] = oldLeft; // New Top = Old Left
						}
						for (int j = 0; j < col.pathCount; j++) {
							for (int k = 0; k < col.GetPath (j).Length; k++) {
								float nodeX = col.GetPath (j) [k].x;
								float nodeY = col.GetPath (j) [k].y;
								col.GetPath (j) [k] = new Vector2 (-nodeY, nodeX);
							}
						}
					}
				}
			}
		}
	}

	// A get/set method for the _Rotation variable that returns a string rather than a value.
	// Used so the rotation has to be set using one of four values, instead of any integer,
	// since setting it anything other than 0, 90, 180 or 270 isn't allowed.
	// Refreshes the segment's facing after a new value is set.
	public string rotation {
		get {
			if (StaticGameCore.rotDict.ContainsValue (_Rotation)) {
				foreach (string key in StaticGameCore.rotDict.Keys) {
					float val;
					StaticGameCore.rotDict.TryGetValue (key, out val);
					if (_Rotation == val) {
						return key;
					}
				}
				return "Invalid rotation value.";
			} else {
				return "Invalid rotation value.";
			}
		}
		set {
			float val;
			if (StaticGameCore.rotDict.TryGetValue (value, out val)) {
				_Rotation = val;
				RefreshFacing ();
			} else {
				Debug.Log ("Unknown value for facing.");
				RefreshFacing ();
			}
		}
	}

	// Finds all segments that are connected to the input segment and puts them in a list you provide.
	private void FindAllConnected (ShipSegment seg, List<ShipSegment> connected)
	{
		//seg.RefreshNeighbors ();
		foreach (ShipSegment neighbor in seg.neighbors) {
			if (!connected.Contains (neighbor)) {
				connected.Add (neighbor);
				FindAllConnected (neighbor, connected);
			}
		}
	}

	// This should probably be split up into at least two separate functions.
	public void TakeDamage (int dmg)
	{
		if (currentIntegrity > dmg) {
			currentIntegrity -= dmg;
			float dmgclr = (float)currentIntegrity / (float)integrity;
			GetComponent<SpriteRenderer> ().color = new Color (dmgclr, dmgclr, dmgclr);
		} else {
			Grid grid = GetComponentInParent<Grid> ();
			Ship ship = GetComponentInParent<Ship> ();

			// this part finds which segment this is in the Grid's segments list, then destroys its slots and removes it from the Grid's list.
			int n = 0;
			for (int i = 0; i < grid.segments.Count; i++) {
				if (grid.segments [i] == this) {
					n = i;
					break;
				}
			}
			for (int i = 0; i < grid.segments.Count; i++) {
				for (int j = 0; j < grid.segments [i]._RelativeGridSlotLocations.Count; j++) {
					grid.DestroySlot (grid.segments [i]._RelativeGridSlotLocations [j] + grid.segments [i].location);
				}
			}
			grid.segments.RemoveAt (n);
			if (grid.segments.Count < 1) {
				GameObject.Destroy (ship.gameObject);
			}
			Destroy (this.gameObject); // This only schedules the destruction of the part, which happens at a later time(Possibly next physics tick? Not sure.). This scheduling is why the script can still continue after this line.

			bool wasCore = false;
			if (this == ship.core) { // Set the ship's core to null if the segment currently being destroyed was the core.
				ship.core = null;
				wasCore = true;
			}

			// This part reforms the parent ship's GridSlots and connections without the segment currently being destroyed.
			grid.PopulateSlots ();
			grid.RefreshConnections ();

			// Goes through all the neighbors of this segment to see if they're connected to the ship's core. Or if they ARE the ship's core.
			for (int i = 0; i < neighbors.Count; i++) {
				if (neighbors [i] == ship.core) {
					Debug.Log (neighbors [i] + " is the Core."); // If the neighbor is the Core itself, ends there.
				} else {
					bool conToCore = grid.CheckIfConnectedToCore (neighbors [i]);
					if (conToCore) {
						//Debug.Log (neighbors [i] + " is connected to the Core."); // If the neighbor is connected to the Core, end there.
					} else {

						// If the neighbor is not connected to the Core...
					//	Debug.Log (neighbors [i] + " is not connected to the Core.");
						List<ShipSegment> derelictSegs = new List<ShipSegment> ();
						List<Vector2> derelictLocs = new List<Vector2> ();
						derelictSegs.Add (neighbors [i]); // Populates the new derelict's segments list by adding all the neighbors of the current neighbor.
						FindAllConnected (neighbors [i], derelictSegs); // Finds all the segments connected to the current neighbor and also adds them to the derelict's segments list.

						// This part gets the location of each segment in the derelict from their location in the old parent ship's grid.
						for (int j = 0; j < grid.segments.Count; j++) {
							if (derelictSegs.Contains (grid.segments [j])) {
								derelictLocs.Add (grid.segments [j].location);
							}
						}
						for (int j = 0; j < derelictSegs.Count; j++) {
							grid.segments.Remove (derelictSegs [j]);
							grid.locations.Remove (derelictLocs [j]);
						}

						// Creates the derelit object, which holds new Grid and Ship scripts.
						GameObject derelict = Instantiate (derelictObj);
						derelict.transform.SetParent (GameObject.FindGameObjectWithTag ("MainCanvas").transform); // Places the derelict on the primary Canvas.

						// Makes sure the derelict is positioned properly.
						derelict.transform.position = ship.transform.position;
						derelict.transform.rotation = ship.transform.rotation;
						derelict.transform.localScale = ship.transform.localScale;

						// Puts the derelict's segments into its Grid and makes them children of the derelict gameobject.
						Grid derelictGrid = derelict.GetComponent<Grid> ();
						for (int j = 0; j < derelictSegs.Count; j++) {
							derelictGrid.segments.Add (derelictSegs [j]);
							derelictGrid.locations.Add (derelictSegs [j].location);
							derelictSegs [j].transform.SetParent (derelict.transform);
						}
					
					
						// Sets the derelict's new core to be the current neighbor of the destroyed segment, then sets the derelicts GridSlots, connections, facing, collider, mass, etc.
						Ship derelictShip = derelict.GetComponent<Ship> ();
						derelictShip.core = derelictGrid.segments [0];
						foreach (ShipSegment seg in derelictGrid.segments) {
							seg.RefreshFacing ();
						}
						derelictGrid.PopulateSlots ();
						derelictGrid.RefreshConnections ();
						derelictShip.RefreshSystems ();
						derelictShip.RefreshCollider ();
						derelictShip.RefreshMass ();


						// Sets the derelict's velocity and rotation to match that of the parent ship's.
						// * The newly applied forces seem a little off, but I'm not sure how to fix that.
						Rigidbody2D deRBody = derelict.GetComponent<Rigidbody2D> ();
						Rigidbody2D paRBody = transform.parent.GetComponent<Rigidbody2D> ();
						deRBody.velocity = paRBody.velocity;
						deRBody.angularVelocity = paRBody.angularVelocity;
					}
				}
			}

			// If this segment was the parent ship's core, destroys the parent ship gameobject, leaving only any potential derelicts behind.
			// If not, refreshes the ship to make sure everything is properly set.
			if (wasCore) {
				GameObject.Destroy (ship.gameObject);
			} else {
				grid.PopulateSlots ();
				grid.RefreshConnections ();
				ship.RefreshSystems ();
				ship.RefreshCollider ();
				ship.RefreshMass ();
			}
		}
	}

	public void RotateSegmentLeft() {
		if (rotation == StaticGameCore.left) {
			rotation = StaticGameCore.bottom;
		} else if (rotation == StaticGameCore.top) {
			rotation = StaticGameCore.left;
		} else if (rotation == StaticGameCore.right) {
			rotation = StaticGameCore.top;
		} else if (rotation == StaticGameCore.bottom) {
			rotation = StaticGameCore.right;
		} else {
			Debug.Log ("Current rotation invalid.");
		}
		RefreshFacing ();
	}

	public void RotateSegmentRight() {
		if (rotation == StaticGameCore.left) {
			rotation = StaticGameCore.top;
		} else if (rotation == StaticGameCore.top) {
			rotation = StaticGameCore.right;
		} else if (rotation == StaticGameCore.right) {
			rotation = StaticGameCore.bottom;
		} else if (rotation == StaticGameCore.bottom) {
			rotation = StaticGameCore.left;
		} else {
			Debug.Log ("Current rotation invalid.");
		}
		RefreshFacing ();
	}

	public void MirrorX() {
		_MirrorX = !_MirrorX;
		RefreshFacing ();
	}

	public void MirrorY() {
		_MirrorY = !_MirrorY;
		RefreshFacing ();
	}
		

	public StoredSegment StoreSegment () {
		StoredSegment stSeg = new StoredSegment();
		stSeg.prefab = db.shipSegments.Find (x => x.GetComponent<ShipSegment> ().segName.Equals (segName));
		stSeg.seg = this;
		stSeg.loc = location;
		stSeg.rot = _Rotation;
		stSeg.mirrorX = _MirrorX;
		stSeg.mirrorY = _MirrorY;
		return stSeg;
	}
}