﻿using UnityEngine;
using System.Collections;

public class Thruster : ShipSystem {

	public float thrust;
	public float topSpeed;
	public Vector2 relativeThrustSource;
	public int _Direction;
	private Vector2 direction;

	// Sets the direction of this thruster, taking into account the segment's rotation and mirroring.
	public override void Init () {
		ShipSegment seg = GetComponent<ShipSegment> ();
		_Direction += (int)seg._Rotation / 90;
		while (_Direction > 3) {
			_Direction -= 4;
		}
		if (seg._MirrorX) {
			if (_Direction == 0) {
				_Direction = 2;
			} else if (_Direction == 2) {
				_Direction = 0;
			}
		}
		if (seg._MirrorY) {
			if (_Direction == 1) {
				_Direction = 3;
			} else if (_Direction == 3) {
				_Direction = 1;
			}
		}
		direction = StaticGameCore.directionList [_Direction];
	}

	public Vector2 actDirection { // Use this to change the direction of the thruster.
		get {
			return direction;
		}
	}
}