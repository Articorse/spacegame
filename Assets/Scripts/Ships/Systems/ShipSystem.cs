﻿using UnityEngine;
using System.Collections;

public class ShipSystem : MonoBehaviour {

	public string systemName;
	public float energyGeneration;
	private bool initialized = false;

	public void InitCall () {
		if (!initialized) {
			Init ();
		}
	}

	public virtual void Init () {
	}
}
