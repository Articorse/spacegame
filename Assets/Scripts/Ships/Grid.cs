﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour {

	public List<ShipSegment> segments; // The list of segments this grid contains.
	public List<Vector2> locations; // The locations of the segments. For editor data entry purposes only.
	private List<GridSlot> slots = new List<GridSlot> (); // The list of GridSlots for this grid.
	private Ship ship; // The ship this grid belongs to.

	void Awake () {
		ship = GetComponentInParent<Ship> ();
	}

	// Function to find which slot is at the given location.
	public GridSlot GetSlotAtLocation (Vector2 loc) {
		foreach (GridSlot s in slots) {
			if (s.location == loc) {
				return s;
			}
		}
		return null;
	}

	// Creates (or re-creates) the list of slots for this grid, then places them in their correct locations, setting the correct sides connectable.
	public void PopulateSlots () {
		slots = new List<GridSlot> ();
		for (int i = 0; i < segments.Count; i++) {
			segments[i].transform.localPosition = segments[i].location * StaticGameCore.gridSize; // Position the segment on the grid.
			for (int j = 0; j < segments[i]._RelativeGridSlotLocations.Count; j++) {
				Vector2 loc = new Vector2 ();
				loc = segments[i].location + segments [i]._RelativeGridSlotLocations [j]; // Calculates the location of the slot relative to the ship itself, rather than to the segment it's part of.
				GridSlot newSlot = new GridSlot (segments[i], loc);
				newSlot.SetSideConnectable (StaticGameCore.left, segments[i].conPoints [j] [0]); // Left
				newSlot.SetSideConnectable (StaticGameCore.top, segments[i].conPoints [j] [1]); // Top
				newSlot.SetSideConnectable (StaticGameCore.right, segments[i].conPoints [j] [2]); // Right
				newSlot.SetSideConnectable (StaticGameCore.bottom, segments[i].conPoints [j] [3]); // Bottom
				slots.Add (newSlot);
			}
		}
	}

	// Creates or refreshes the connections between GridSlots, and also re-populates their neighbors lists.
	public void RefreshConnections () {
		foreach (ShipSegment seg in segments) {
			seg.neighbors = new List<ShipSegment> ();
		}
		foreach (GridSlot slot in slots) {
			GridSlot neighbor = GetSlotAtLocation (new Vector2 (slot.location.x - 1, slot.location.y));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.left) && neighbor.IsSideConnectable(StaticGameCore.right)) {
				slot.SetSideConnected (StaticGameCore.left, neighbor);
				List<ShipSegment> segNeighbors = slot.GetSegment.neighbors;
				ShipSegment neighborSegment = neighbor.GetSegment;
				if (!segNeighbors.Contains (neighborSegment)) {
					segNeighbors.Add (neighborSegment);
				}
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x, slot.location.y + 1));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.top) && neighbor.IsSideConnectable(StaticGameCore.bottom)) {
				slot.SetSideConnected (StaticGameCore.top, neighbor);
				List<ShipSegment> segNeighbors = slot.GetSegment.neighbors;
				ShipSegment neighborSegment = neighbor.GetSegment;
				if (!segNeighbors.Contains (neighborSegment)) {
					segNeighbors.Add (neighborSegment);
				}
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x + 1, slot.location.y));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.right) && neighbor.IsSideConnectable(StaticGameCore.left)) {
				slot.SetSideConnected (StaticGameCore.right, neighbor);
				List<ShipSegment> segNeighbors = slot.GetSegment.neighbors;
				ShipSegment neighborSegment = neighbor.GetSegment;
				if (!segNeighbors.Contains (neighborSegment)) {
					segNeighbors.Add (neighborSegment);
				}
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x, slot.location.y - 1));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.bottom) && neighbor.IsSideConnectable(StaticGameCore.top)) {
				slot.SetSideConnected (StaticGameCore.bottom, neighbor);
				List<ShipSegment> segNeighbors = slot.GetSegment.neighbors;
				ShipSegment neighborSegment = neighbor.GetSegment;
				if (!segNeighbors.Contains (neighborSegment)) {
					segNeighbors.Add (neighborSegment);
				}
			}
		}
	}

	// Checks if the given segment has a link to the ship's Core. Used to determine which segments remain part of a ship, and which segments split off to form a derelict, when a segment is destroyed.
	public bool CheckIfConnectedToCore (ShipSegment seg) {
		if (segments.Contains (seg)) {
			if (ship.core == null) {
				return false;
			} else if (ship.core == seg) {
				return true;
			}
			ShipSegment curSeg = ship.core;
			List<ShipSegment> checkedSegments = new List<ShipSegment> ();
			Vector2 absLoc = ship.core.location;
			if (CheckIfConnectedToCoreRecursion (seg, curSeg, checkedSegments, absLoc)) {
				return true;
			}
			return false;
		} else {
	//		Debug.Log (seg.name + " is not part of this Grid.");
			return false;
		}
	}

	// The recursive part of the above function.
	private bool CheckIfConnectedToCoreRecursion (ShipSegment seg, ShipSegment curSeg, List<ShipSegment> checkedSegments, Vector2 absLoc) {
		checkedSegments.Add (curSeg);
		List<ShipSegment> neighbors = new List<ShipSegment> ();
		for (int i = 0; i < curSeg._RelativeGridSlotLocations.Count; i++) {
			GridSlot curSlot = GetSlotAtLocation (absLoc + curSeg._RelativeGridSlotLocations [i]);
			for (int k = 0; k < 4; k++) {
				GridSlot potentialNeighborSlot = curSlot.SideConnectedTo (k);
				if (potentialNeighborSlot != null) {
					ShipSegment potentialNeighborSegment = potentialNeighborSlot.GetSegment;
					if (potentialNeighborSegment == seg) {
						return true;
					}
					if (potentialNeighborSlot != null && potentialNeighborSegment != curSeg && !neighbors.Contains (potentialNeighborSegment)) {
						neighbors.Add (potentialNeighborSegment);
					}
				}
			}
		}
		for (int i = 0; i < neighbors.Count; i++) {
			for (int j = 0; j < segments.Count; j++) {
				if (neighbors [i] == segments [j]) {
					absLoc = segments [j].location;
				}
			}
			if (!checkedSegments.Contains (neighbors [i])) {
				if (CheckIfConnectedToCoreRecursion (seg, neighbors [i], checkedSegments, absLoc)) {
					return true;
				}
			}
		}
		return false;
	}

	public bool CheckIfValidPlacement (ShipSegment seg, Vector2 loc) {
		seg.RefreshFacing ();
		List<Vector2> relGridSlotLocationsOnGrid = new List<Vector2> ();
		for (int i = 0; i < seg._RelativeGridSlotLocations.Count; i++) {
			relGridSlotLocationsOnGrid.Add(loc + seg._RelativeGridSlotLocations[i]);
			if (GetOccupiedSlots.Contains (relGridSlotLocationsOnGrid[relGridSlotLocationsOnGrid.Count-1])) {
				return false;
			}
		}
		List<GridSlot> segSlots = new List<GridSlot> ();
		for (int j = 0; j < seg._RelativeGridSlotLocations.Count; j++) {
			Vector2 slotLoc = new Vector2 ();
			slotLoc = loc + seg._RelativeGridSlotLocations [j]; // Calculates the location of the slot relative to the ship itself, rather than to the segment it's part of.
			GridSlot newSlot = new GridSlot (seg, slotLoc);
			newSlot.SetSideConnectable (StaticGameCore.left, seg.conPoints [j] [0]); // Left
			newSlot.SetSideConnectable (StaticGameCore.top, seg.conPoints [j] [1]); // Top
			newSlot.SetSideConnectable (StaticGameCore.right, seg.conPoints [j] [2]); // Right
			newSlot.SetSideConnectable (StaticGameCore.bottom, seg.conPoints [j] [3]); // Bottom
			segSlots.Add (newSlot);
		}
		foreach (GridSlot slot in segSlots) {
			GridSlot neighbor = GetSlotAtLocation (new Vector2 (slot.location.x - 1, slot.location.y));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.left) && neighbor.IsSideConnectable(StaticGameCore.right)) {
				return true;
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x, slot.location.y + 1));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.top) && neighbor.IsSideConnectable(StaticGameCore.bottom)) {
				return true;
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x + 1, slot.location.y));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.right) && neighbor.IsSideConnectable(StaticGameCore.left)) {
				return true;
			}
			neighbor = GetSlotAtLocation (new Vector2 (slot.location.x, slot.location.y - 1));
			if (neighbor != null && slot.IsSideConnectable (StaticGameCore.bottom) && neighbor.IsSideConnectable(StaticGameCore.top)) {
				return true;
			}
		}
		return false;
	}

	// Remove a given slot from the slots list and makes it null.
	public void DestroySlot (GridSlot slot) {
		slots.Remove (slot);
		slot = null;
	}

	// Same as above, but removes a slot given only its location.
	public void DestroySlot (Vector2 slotLocation) {
		var slot = GetSlotAtLocation (slotLocation);
		slots.Remove (slot);
		slot = null;
	}

	public List<Vector2> GetOccupiedSlots {
		get {
			List<Vector2> locs = new List<Vector2> ();
			foreach (GridSlot slot in slots) {
				locs.Add (slot.location);
			}
			return locs;
		}
	}
}