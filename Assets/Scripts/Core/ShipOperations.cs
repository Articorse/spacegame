﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipOperations : MonoBehaviour {

	public static Ship SpawnShip (Vector2 pos, Quaternion rot) {
		Transform mainCanvas = GameObject.FindGameObjectWithTag ("MainCanvas").transform;
		GameObject shipObj = (GameObject)Instantiate (GlobalCore.Instance.shipPrefab, pos, rot, mainCanvas);
		return shipObj.GetComponent<Ship> ();
	}
}