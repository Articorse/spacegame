﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataBase : MonoBehaviour {

	public List<GameObject> shipSegments;
	public StoredShip stShip;
	public List<GameObject> shipObjInventory = new List<GameObject> ();
	private List<StoredShip> shipInventory = new List<StoredShip> ();

	void Awake () {
		foreach (GameObject shipObj in shipObjInventory) {
			GameObject curShip = Instantiate (shipObj);
			Ship s = curShip.GetComponent<Ship> ();
			shipInventory.Add (s.StoreShip ());
			DestroyImmediate (curShip);
		}
	}

	public List<StoredShip> ShipList {
		get {
			return shipInventory;
		}
	}

	public StoredShip GetShip (int id) {
		return shipInventory [id];
	}

	public void AddShip (StoredShip ship) {
		if (!ShipList.Contains (ship)) {
			shipInventory.Add (ship);
		} else {
			Debug.Log ("Ship already in list.");
		}
	}

	public void ReplaceShip (StoredShip oldShip, StoredShip newShip) {
		if (shipInventory.Contains (oldShip)) {
			int id = shipInventory.IndexOf (oldShip);
			shipInventory [id] = newShip;
		} else {
			Debug.Log ("The selected ship is not part of the ship inventory.");
		}
	}

	public void DeleteShip (StoredShip ship) {
		if (shipInventory.Contains (ship)) {
			int id = shipInventory.IndexOf (ship);
			shipInventory [id] = null;
			shipInventory.RemoveAt (id);
		} else {
			Debug.Log ("The selected ship is not part of the ship inventory.");
		}
	}
}