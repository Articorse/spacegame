﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GlobalCore : MonoBehaviour {

	public static GlobalCore Instance;
	public GameObject shipPrefab;
	public int playerShipId = 0;
	public StoredShip selectedShip;

	void Awake ()
	{
		if (Instance == null) {
			DontDestroyOnLoad (gameObject);
			Instance = this;
		} else if (Instance != this) {
			Destroy (gameObject);
		}
	}

	void Start () {
		selectedShip = null;
	}
}