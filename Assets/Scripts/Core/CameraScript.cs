﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	private GameObject player;
	private Rigidbody2D playerRBody;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerRBody = player.GetComponent<Rigidbody2D> ();
		transform.position = StaticGameCore.defCamPos;
	}
	
	// Locks the camera onto the player ship's center of mass.
	void FixedUpdate () {
		Vector3 newPos = new Vector3(playerRBody.worldCenterOfMass.x, playerRBody.worldCenterOfMass.y, StaticGameCore.defCamPos.z);
		transform.position = newPos;
	}
}
