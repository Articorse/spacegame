﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StaticGameCore : MonoBehaviour {

	// A collection of global constants and dictionaries.
	public static Vector3 defCamPos = new Vector3(0, 0, -50);
	public static int gridSize = 30;
	public static int globalTopSpeed = 2000;
	public static int globalRotTopSpeed = 1000;
	public static int rotStabilizationTreshold = 20;
	public static int vectStabilizationTresholdSqr = 0;
	public static float vectXTreshold = 1;
	public static string top = "Top";
	public static string bottom = "Bottom";
	public static string left = "Left";
	public static string right = "Right";
	public static string mirrorNone = "Not Mirrored";
	public static string mirrorX = "Mirrored X";
	public static string mirrorY = "Mirrored Y";
	public static string mirrorBoth = "Mirrored X and Y";
	public static Dictionary<string, Vector2> vectDict = new Dictionary<string, Vector2> {
		{left, Vector2.left},
		{top, Vector2.up},
		{right, Vector2.right},
		{bottom, Vector2.down}
	};
	public static Dictionary<string, float> rotDict = new Dictionary<string, float> {
		{top, 0},
		{left, 90},
		{bottom, 180},
		{right, 270} 
	};
	public static Dictionary<string, Vector2> mirrorDict = new Dictionary<string, Vector2> {
		{mirrorNone, new Vector2(1, 1)},
		{mirrorX, new Vector2(-1, 1)},
		{mirrorY, new Vector2(1, -1)},
		{mirrorBoth, new Vector2(-1, -1)},
	};
	public static List<Vector2> directionList = new List<Vector2> {
		{new Vector2(-1, 0)},		
		{new Vector2(0, 1)},
		{new Vector2(1, 0)},
		{new Vector2(0, -1)},
	};
}