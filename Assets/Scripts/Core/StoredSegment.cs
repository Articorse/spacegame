﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoredSegment {

	public GameObject prefab;
	public ShipSegment seg;
	public Vector2 loc;
	public float rot;
	public bool mirrorX;
	public bool mirrorY;
}

public class StoredShip {

	public List<StoredSegment> segments;
	public int core;
}