﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldPosUI : MonoBehaviour {

	public GameObject xPosValue;
	public GameObject yPosValue;
	private GameObject player;
	private Ship playerShip;
	private Text xPosText;
	private Text yPosText;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerShip = player.GetComponent<Ship> ();
		xPosText = xPosValue.transform.GetComponent<Text> ();
		yPosText = yPosValue.transform.GetComponent<Text> ();

	}
	
	// Makes sure the X and Y coordinates on the UI always display the player's current location.
	void FixedUpdate () {
		xPosText.text = playerShip.x.ToString();
		yPosText.text = playerShip.y.ToString();
	}
}