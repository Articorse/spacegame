﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Pause : MonoBehaviour {

	private GameObject[] pauseObjects;

	void Start () {
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag ("PauseScreen");
		hidePaused ();
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			pauseControl ();
		}
	}

	public void Reload() {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void GoToShipSelection() {
		SceneManager.LoadScene ("ShipSelection");
	}

	public void pauseControl () {
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
			showPaused ();
		} else if (Time.timeScale == 0) {
			Time.timeScale = 1;
			hidePaused ();
		}
	}

	public void showPaused () {
		foreach (GameObject obj in pauseObjects) {
			obj.SetActive (true);
		}
	}

	public void hidePaused () {
		foreach (GameObject obj in pauseObjects) {
			obj.SetActive (false);
		}
	}
}
