﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnergyDisplay : MonoBehaviour {

	private GameObject player;
	private Ship playerShip;
	private Text energyValue;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerShip = player.GetComponent<Ship> ();
		energyValue = GetComponent<Text> ();
	}

	void Update () {
		energyValue.text = Mathf.Round (playerShip.curEnergy).ToString () + " / " + Mathf.Round (playerShip.energyCapacity).ToString ();
	}
}