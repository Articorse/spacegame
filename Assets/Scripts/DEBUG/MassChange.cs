﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// This script is only for testing purposes. It controls the mass modifier in the bottom right of the UI.
public class MassChange : MonoBehaviour {

	public GameObject massOutput;
	private GameObject player;
	private Text massOutputText;
	private Rigidbody2D rBody;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		massOutputText = massOutput.GetComponent<Text> ();
		rBody = player.GetComponent<Rigidbody2D> ();
	}
	
	void Update () {
		massOutputText.text = rBody.mass.ToString ();
	}

	public void Minus () {
		if (rBody.mass > 1) {
			rBody.mass -= 1;
		}
	}

	public void Plus () {
		rBody.mass += 1;
	}
}