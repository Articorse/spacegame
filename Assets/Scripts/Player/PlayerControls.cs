﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerControls : MonoBehaviour {

	private Ship playerShip;
	private float zoomLevel;
	private DataBase db;

	/////////// These values are used to show the player ship's coordinates,
	// DEBUG // velocity and rotational velocity, as well as messages displayed
	/////////// when turning the stabilization systems on and off.
	public GameObject veloMagValue;
	public GameObject angVeloValue;
	public GameObject messagesValue;
	public Slider slider;
	private Text veloMagText;
	private Text angVeloText;
	private Text messagesText;
	private Color messageColor;
	private Rigidbody2D rBody;
	///////////
	// DEBUG //
	///////////

	void Awake () {
		db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
		GlobalCore gCore = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<GlobalCore> ();
		playerShip = ShipOperations.SpawnShip (transform.position, transform.rotation);
		playerShip.tag = "Player";
		playerShip.PopulateShip (db.GetShip(gCore.playerShipId));
		zoomLevel = Camera.main.orthographicSize;
		///////////
		// DEBUG //
		///////////
		rBody = playerShip.transform.GetComponent<Rigidbody2D> ();
		if (veloMagValue != null) {
			veloMagText = veloMagValue.transform.GetComponent<Text> ();
		}
		if (angVeloValue != null) {
			angVeloText = angVeloValue.transform.GetComponent<Text> ();
		}
		if (messagesValue != null) {
		messagesText = messagesValue.transform.GetComponent<Text> ();
		messageColor = messagesText.color;
		messageColor.a = 0f;
			messagesText.color = new Color (messageColor.r, messageColor.r, messageColor.b, 0f);
		}
		///////////
		// DEBUG //
		///////////

		}

	// The Update() listens for button presses to turn the stabilization systems on and off.
	// It also sends a string to the Messages UI field.
	void Update () {
		if (Input.GetKeyDown (KeyCode.R)) {
			playerShip.ToggleRotStab ();
			if (playerShip.rotStab) {
				if (messagesValue != null) {
					messagesText.text = "Rotational Stabilizers turned on.";
				}
			} else {
				if (messagesValue != null) {
					messagesText.text = "Rotational Stabilizers turned off.";
				}
			}
			if (messagesValue != null) {
				messageColor.a = 1f;
			}
		}
		if (Input.GetKeyDown (KeyCode.I)) {
			playerShip.ToggleVectStab ();
			if (playerShip.vectStab) {
				if (messagesValue != null) {
					messagesText.text = "Vector Stabilizers turned on.";
				}
			} else {
				if (messagesValue != null) {
					messagesText.text = "Vector Stabilizers turned off.";
				}
			}
			if (messagesValue != null) {
				messageColor.a = 1f;
			}
		}
		if (messagesValue != null) {
			messagesText.color = new Color (messageColor.r, messageColor.g, messageColor.b, messageColor.a); // Sets the color and opacity of the Messages field to the default from above.
		}
		if (messagesValue != null) {
			messageColor.a = Mathf.Lerp (messageColor.a, 0, 0.005f); // Gradually fades the text out until it's invisible.
		}
		if (slider != null) {
			slider.value = playerShip.curEnergy / playerShip.energyCapacity;
		}
		if (Input.GetKeyDown (KeyCode.C)) {
			StoredShip stShip = playerShip.StoreShip ();
			Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Ship newShip = ShipOperations.SpawnShip (pos, Quaternion.Euler(0, 0, 0));
			newShip.PopulateShip (stShip);
		}

		//Scrollwheel Zoom
		var d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			zoomLevel -= zoomLevel/15;
		}
		else if (d < 0f)
		{
			// scroll down
			zoomLevel += zoomLevel/15;
		}

		if (Mathf.Abs (Camera.main.orthographicSize - zoomLevel) > 0.01) {
			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, zoomLevel, Time.deltaTime * 10);
		}

		//Firing Weapons)
		if (Input.GetButton ("Fire1")) {
			playerShip.Fire1Held = true;
		} else {
			playerShip.Fire1Held = false;
		}

	}

	void FixedUpdate () {

		///////////
		// DEBUG // This part sends the player's velocity and rotational velocity to the UI display.
		///////////
		if (veloMagValue != null) {
			veloMagText.text = rBody.velocity.magnitude.ToString ();
		}
		if (angVeloValue != null) {
			angVeloText.text = rBody.angularVelocity.ToString ();
		}
		///////////
		// DEBUG //
		///////////

		// This part checks for any input from the player. If there is no forward or backward input,
		// the passive vector stabilization is called, which essentially acts as a brake.
		// A similar thing happens to the rotation, as well.
		if (Input.GetAxis ("Vertical") > 0) {
			playerShip.Accelerate (StaticGameCore.top);
		} else {
			playerShip.PassiveVectorStabilization ();
		} 
		if (Input.GetAxis ("Vertical") < 0) {
			playerShip.Accelerate (StaticGameCore.bottom);
		} else {
			playerShip.PassiveVectorStabilization ();
		}
		if (Input.GetAxis ("Strafe") < 0) {
			playerShip.Accelerate (StaticGameCore.left);
		} 
		if (Input.GetAxis ("Strafe") > 0) {
			playerShip.Accelerate (StaticGameCore.right);
		} 
		playerShip.StabilizeVector ();
		float rotInput = Input.GetAxis ("Horizontal");
		if (rotInput != 0) {
			playerShip.Rotate (rotInput);
		}
		playerShip.StabilizeRotation ();
		playerShip.LimitRotation (); // Forbids the ship from rotating faster than the top rotational speed of the stabilization system.
	}
}