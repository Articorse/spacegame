﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class ShipInventory : MonoBehaviour {

	public GameObject slot;
	public int rows;
	public int columns;
	public int firstX;
	public int firstY;
	public int xDist;
	public int yDist;

	public ShipInvSlot selectedSlot;

	private DataBase db;
	private GlobalCore gCore;
	private List<ShipInvSlot> slots = new List<ShipInvSlot> ();

	void Start () {
		db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
		gCore = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<GlobalCore> ();
		RefreshInventory ();
	}

	void RefreshInventory ()
	{
		if (slots != null) {
			for (int i = 0; i < slots.Count; i++) {
				Destroy (slots[i].gameObject);
			}
		}
		slots = new List<ShipInvSlot> ();
		int c = 0;
		for (int j = 0; j < rows; j++) {
			for (int i = 0; i < columns; i++) {
				GameObject newSlot = (GameObject)Instantiate (slot, transform);
				newSlot.transform.localPosition = new Vector2 (firstX + i * xDist, firstY - j * yDist);
				newSlot.transform.localScale = new Vector3 (1, 1, 1);
				newSlot.name = "Slot " + i.ToString () + " " + j.ToString ();
				ShipInvSlot invSlot = newSlot.GetComponent<ShipInvSlot> ();
				invSlot.id = i + j * i;
				if (c < db.ShipList.Count) {
					invSlot.InitCall (db.ShipList [c]);
					c++;
				}
				else {
					invSlot.InitCall (null);
					invSlot.Disable ();
				}
				slots.Add (invSlot);
			}
		}
	}

	public void Select (ShipInvSlot s) {
		if (s._Ship != null) {
			foreach (ShipInvSlot iS in slots) {
				iS.GetComponent<Image> ().color = Color.white;
			}
			s.GetComponent<Image> ().color = Color.gray;
			selectedSlot = s;
		}
	}

	public void Play () {
		if (selectedSlot != null) {
			gCore.playerShipId = selectedSlot.id;
			SceneManager.LoadScene ("Main");
		}
	}
	
	public void Edit () {
		if (selectedSlot != null) {
			gCore.selectedShip = selectedSlot._Ship;
			SceneManager.LoadScene ("ShipEditor");
		}
	}

	public void NewShip () {
		gCore.selectedShip = null;
		SceneManager.LoadScene ("ShipEditor");
	}

	public void DeleteShip () {
		if (selectedSlot != null) {
			db.DeleteShip (selectedSlot._Ship);
			RefreshInventory ();
		}
	}
}