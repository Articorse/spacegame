﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class SegmentInventory : MonoBehaviour {

	public GameObject slot;
	public float invHeightMax;
	public int firstX;
	public int firstY;
	public int xDist;
	public int yDist;
	public Ship editedShip;

	public SegmentInvSlot selectedSlot;
	public GameObject selectedSegmentObj;

	public Color red;
	public Color green;

	public int startFromNumber;

	private DataBase db;
	private List<SegmentInvSlot> slots = new List<SegmentInvSlot> ();
	private ShipSegment selSegm;
	private int halfGridSpace;
	private GameObject mainCanvas;
	private Grid grid;
	private Rect rect;

	private GlobalCore gCore;

	void Start () {
		startFromNumber = 0;
		grid = editedShip.GetComponent<Grid> ();
		mainCanvas = GameObject.FindGameObjectWithTag ("MainCanvas");
		halfGridSpace = StaticGameCore.gridSize / 2;
		selSegm = null;
		db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
		rect = transform.GetComponent<RectTransform> ().rect;
		gCore = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<GlobalCore> ();
		if (gCore.selectedShip != null) {
			editedShip.PopulateShip (gCore.selectedShip);
			editedShip.TotalRefresh ();
		}
		RefreshSlots ();
	}

	private void CheckValidPlacement (Vector2 pos) {
		if (grid.CheckIfValidPlacement (selSegm, pos)) {
			selectedSegmentObj.GetComponent<SpriteRenderer> ().color = green;
		} else {
			selectedSegmentObj.GetComponent<SpriteRenderer> ().color = red;
		}
	}

	private void Update () {
		if (selSegm != null) {
			
			Vector2 pos = (Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition);
			int mod = 0;
			int div = Math.DivRem ((int)pos.x, StaticGameCore.gridSize, out mod);
			if (mod > halfGridSpace) {
				div++;
			}
			int posX = div * StaticGameCore.gridSize;

			div = Math.DivRem ((int)pos.y, StaticGameCore.gridSize, out mod);
			if (mod > halfGridSpace) {
				div++;
			}
			int posY = div * StaticGameCore.gridSize;
			pos = new Vector2 (posX, posY);
			if ((Vector2)selSegm.transform.position != pos) {
				selSegm.transform.position = pos;
				if (grid.segments.Count > 0) {
					pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
					CheckValidPlacement (pos);
				} else {
					selectedSegmentObj.GetComponent<SpriteRenderer> ().color = green;
				}
			}



			if (Input.GetKeyDown (KeyCode.Escape)) {
				Deselect ();

			}

			if (Input.GetKeyDown (KeyCode.LeftBracket)) {
				if (selSegm.gameObject != null) {
					selSegm.RotateSegmentLeft ();
					pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
					CheckValidPlacement (pos);
				}
			}

			if (Input.GetKeyDown (KeyCode.RightBracket)) {
				if (selSegm.gameObject != null) {
					selSegm.RotateSegmentRight ();
					pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
					CheckValidPlacement (pos);
				}
			}

			if (Input.GetKeyDown (KeyCode.Quote)) {
				if (selSegm.gameObject != null) {
					selSegm.MirrorY ();
					pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
					CheckValidPlacement (pos);
				}
			}

			if (Input.GetKeyDown (KeyCode.Slash)) {
				if (selSegm.gameObject != null) {
					selSegm.MirrorX ();
					pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
					CheckValidPlacement (pos);
				}
			}

			if (Input.GetKeyDown (KeyCode.Mouse0)) {
				float x = Input.mousePosition.x;
				if (x > rect.width / 2) {
					TryToPlaceSegment (pos);
					RefreshSlots ();
					if (selSegm != null) {
						pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
						CheckValidPlacement (pos);
					}
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.Mouse1)) {
			Vector2 pos = (Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition);
			int mod = 0;
			int div = Math.DivRem ((int)pos.x, StaticGameCore.gridSize, out mod);
			if (mod > halfGridSpace) {
				div++;
			}
			int posX = div * StaticGameCore.gridSize;

			div = Math.DivRem ((int)pos.y, StaticGameCore.gridSize, out mod);
			if (mod > halfGridSpace) {
				div++;
			}
			int posY = div * StaticGameCore.gridSize;
			pos = new Vector2 (posX, posY);
			pos = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
			DestroySegmentAtPosition (pos);
			if (selSegm != null) {
				CheckValidPlacement (pos);
			}
		}
	}

	public void Deselect () {
		if (selSegm.gameObject != null) {
			DestroyImmediate (selSegm.gameObject);
			selSegm = null;
			selectedSegmentObj = null;
			selectedSlot.GetComponent<Image> ().color = Color.white;
			selectedSlot = null;
			RefreshSlots ();
		}
	}

	public void Select (SegmentInvSlot s) {
		if (s.isSelectable) {
			foreach (SegmentInvSlot iS in slots) {
				if (iS._Segment != null) {
					iS.GetComponent<Image> ().color = Color.white;
				} else {
					iS.GetComponent<Image> ().color = Color.gray;
				}
			}
			s.GetComponent<Image> ().color = Color.gray;
			selectedSlot = s;

			if (selectedSegmentObj != null) {
				DestroyImmediate (selectedSegmentObj);
			}
			selectedSegmentObj = (GameObject)Instantiate (s._Segment);
			selectedSegmentObj.transform.localScale = new Vector3 (1, 1, 1);
			if (editedShip.GetComponent<Grid> ().segments.Count > 0) {
				selectedSegmentObj.GetComponent<SpriteRenderer> ().color = red;
			} else {
				selectedSegmentObj.GetComponent<SpriteRenderer> ().color = green;
			}
			selSegm = selectedSegmentObj.GetComponent<ShipSegment> ();
			RefreshSlots ();
		}
	}

	public void SaveShip () {
		if (editedShip.CheckIfShipIsValid ()) {
			StoredShip stShip = editedShip.StoreShip ();
			DataBase db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
			if (gCore.selectedShip != null) {
				db.ReplaceShip (gCore.selectedShip, stShip);
			} else {
				db.AddShip (stShip);
			}
			SceneManager.LoadScene ("ShipSelection");
		} else {
		}
	}

	public void CancelChanges () {
		DataBase db = GameObject.FindGameObjectWithTag ("GameCore").GetComponent<DataBase> ();
		gCore.selectedShip = null;
		SceneManager.LoadScene ("ShipSelection");
	}

	public ShipSegment TryToPlaceSegment(Vector2 pos, bool deselect = true) {
		if (selSegm != null) {
			ShipSegment newSeg = null;
			if (grid.segments.Count < 1) {
				selSegm.location = new Vector2 (0, 0);
				StoredSegment stSeg = selSegm.StoreSegment ();
				editedShip.transform.position = pos;
				newSeg = editedShip.AddSegment (stSeg);
				newSeg.RefreshFacing ();
				editedShip.TotalRefresh ();
				RefreshSlots ();
			} else {
				selSegm.location = (pos - (Vector2)editedShip.transform.position) / StaticGameCore.gridSize;
				StoredSegment stSeg = selSegm.StoreSegment ();
				newSeg = editedShip.AddSegment (stSeg);
				if (newSeg != null) {
					newSeg.RefreshFacing ();
					editedShip.TotalRefresh ();
					if (newSeg.neighbors.Count < 1) {
						grid.segments.Remove (newSeg);
						DestroyImmediate (newSeg.gameObject);
					}
				}
				RefreshSlots ();
			}
			if (editedShip.core == null && newSeg.type.Contains("Cockpit")) {
				editedShip.core = newSeg;
				if (deselect) {
					Deselect ();
				}
			}
			return newSeg;
		}
		return null;
	}

	public void DestroySegmentAtPosition (Vector2 pos)	{
		GridSlot gridSlot = grid.GetSlotAtLocation (pos);
		if (gridSlot != null) {
			ShipSegment getSegment = gridSlot.GetSegment;
			if (editedShip.core == getSegment && selSegm != null && !selSegm.type.Contains("Cockpit")) {
				Deselect ();
			}
			grid.segments.Remove (getSegment);
			DestroyImmediate (getSegment.gameObject);
			editedShip.TotalRefresh ();
			RefreshSlots ();
		}
	}

	public void RefreshSlots ()	{
		if (slots.Count > 0) {
			foreach (SegmentInvSlot slot in slots) {
				DestroyImmediate (slot.GetComponent<GameObject> ());
			}
		}
		slots = new List<SegmentInvSlot> ();
		int j = 0;
		int c = startFromNumber;
		while (rect.height * invHeightMax > Mathf.Abs (firstY - j * yDist)) {
			int i = 0;
			while (rect.width > firstX + i * xDist) {
				GameObject newSlot = (GameObject)Instantiate (slot, transform);
				newSlot.transform.localPosition = new Vector2 (firstX + i * xDist, firstY - j * yDist);
				newSlot.transform.localScale = new Vector3 (1, 1, 1);
				newSlot.name = "Slot " + i.ToString () + " " + j.ToString ();
				SegmentInvSlot invSlot = newSlot.GetComponent<SegmentInvSlot> ();
				invSlot.id = i + j * i;
				if (c < db.shipSegments.Count) {
					invSlot.InitCall (db.shipSegments [c]);
					c++;
				} else {
					invSlot.InitCall (null);
					invSlot.MakeEmpty ();
					invSlot.GetComponent<Image> ().color = Color.gray;
				}
				slots.Add (invSlot);
				i++;
			}
			j++;
		}
		foreach (SegmentInvSlot slot in slots) {
			if (slot._Segment != null) {
				slot.MakeSelectable ();
			} else {
				slot.MakeUnselectable ();
			}
		}
		if (editedShip.core != null) {
			foreach (SegmentInvSlot slot in slots) {
				if (slot._Segment != null) {
					if (slot._Segment.GetComponent<ShipSegment> ().type.Contains ("Cockpit")) {
						slot.MakeUnselectable ();
					}
				}
			}
		}
		if (grid.segments.Count < 1) {
			foreach (SegmentInvSlot slot in slots) {
				if (slot._Segment != null) {
					if (!slot._Segment.GetComponent<ShipSegment> ().type.Contains ("Cockpit")) {
						slot.MakeUnselectable ();
					}
				}
			}
		}
		foreach (ShipSegment s in grid.segments) {
			if (!grid.CheckIfConnectedToCore (s)) {
				s.GetComponent<SpriteRenderer> ().color = red;
			} else {
				s.GetComponent<SpriteRenderer> ().color = Color.white;
			}
		}
	}
}