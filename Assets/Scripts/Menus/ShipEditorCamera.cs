﻿using UnityEngine;
using System.Collections;

public class ShipEditorCamera : MonoBehaviour {

	public float dragSensitivity;
	private float zoomLevel;
	private Vector3 defCamPos;
	private float defCamOrthSize;
	private Rect inv;

	void Awake () {
		defCamPos = transform.position;
		zoomLevel = Camera.main.orthographicSize;
		defCamOrthSize = Camera.main.orthographicSize;
		inv = GameObject.FindGameObjectWithTag ("SegmentInventory").GetComponent<RectTransform> ().rect;
	}

	void Update () {
		if (Input.GetKey (KeyCode.Mouse2)) {
			Vector3 mPos = Input.mousePosition;
			if (mPos.x > inv.width / 2) {
				float mouseX = Input.GetAxis ("Mouse X");
				float mouseY = Input.GetAxis ("Mouse Y");
				Vector3 cameraPos = new Vector3 (-mouseX * dragSensitivity, -mouseY * dragSensitivity, 0);
				float zoomDragMultiplier = zoomLevel / defCamOrthSize;
				float newX = Mathf.Lerp (0, cameraPos.x, Time.deltaTime * 10 * zoomDragMultiplier);
				float newY = Mathf.Lerp (0, cameraPos.y, Time.deltaTime * 10 * zoomDragMultiplier);
				Vector3 newCameraPos = new Vector3 (newX, newY, 0);
				transform.position += newCameraPos;
			}
		}

		if (Input.GetKeyDown (KeyCode.C)) {
			transform.position = defCamPos;
			zoomLevel = defCamOrthSize;
			Camera.main.orthographicSize = defCamOrthSize;
		}

		//Scrollwheel Zoom
		float d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			zoomLevel -= zoomLevel/15;
		}
		else if (d < 0f)
		{
			// scroll down
			zoomLevel += zoomLevel/15;
		}

		float camOrthSize = Camera.main.orthographicSize;
		if (Mathf.Abs (camOrthSize - zoomLevel) > 0.01) {
			Camera.main.orthographicSize = Mathf.Lerp (camOrthSize, zoomLevel, Time.deltaTime * 10);
		}
	}
}
