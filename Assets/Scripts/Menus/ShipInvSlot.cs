﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ShipInvSlot : MonoBehaviour {

	public StoredShip _Ship;
	public int id;
	private Image shipThumbnail;
	private ShipInventory inv;

	public void InitCall (StoredShip ship) {
		inv = transform.parent.GetComponent<ShipInventory> ();
		shipThumbnail = transform.GetChild (0).GetComponent<Image> ();
		if (ship != null) {
			_Ship = ship;
			shipThumbnail.enabled = true;
		}
	}

	public void Select() {
		inv.Select (this);
	}

	public void Disable() {
		_Ship = null;
		shipThumbnail.enabled = false;
	}
}