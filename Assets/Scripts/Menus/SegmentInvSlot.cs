﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SegmentInvSlot : MonoBehaviour {

	public GameObject _Segment;
	public int id;
	private Image segThumbnail;
	private SegmentInventory inv;
	private Image slotBackground;
	private bool selectable;

	public void InitCall (GameObject seg) {
		inv = transform.parent.GetComponent<SegmentInventory> ();
		slotBackground = GetComponent<Image> ();
		segThumbnail = transform.GetChild (0).GetComponent<Image> ();
		if (seg != null) {
			_Segment = seg;
			segThumbnail.enabled = true;
			segThumbnail.sprite = _Segment.GetComponent<SpriteRenderer> ().sprite;
			selectable = true;
		} else {
			selectable = false;
		}
	}

	public void Select() {
		inv.Select (this);
	}

	public void MakeEmpty() {
		_Segment = null;
		segThumbnail.enabled = false;
	}

	public void MakeSelectable() {
		selectable = true;
		slotBackground.color = Color.white;
	}

	public void MakeUnselectable() {
		selectable = false;
		slotBackground.color = Color.gray;
	}

	public bool isSelectable {
		get {
			return selectable;
		}
	}
}